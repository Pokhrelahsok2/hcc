<?php

use Illuminate\Support\Facades\Route;
use App\School;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//send school Info to all pages
view()->composer(['*'], function ($view) {
    $school = School::where("id", 1)->first();
    $view->with('school', $school);
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::resource('school', 'SchoolController');
    Route::resource('levels', 'LevelController');
    Route::resource('faculties', 'FacultyController');
    Route::resource('batches', 'BatchController');

    Route::get('students/form/download', 'StudentController@downloadForm');
    Route::post('students/excel/import', 'StudentController@importExcel');
    Route::resource('students', 'StudentController');
    Route::resource('teachers', 'TeacherController');
    Route::resource('fee-topics', 'FeeTopicController');
});

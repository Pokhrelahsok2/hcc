<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function faculty()
    {
        return $this->belongsTo(Faculty::class);
    }
}

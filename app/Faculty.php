<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }
    public function feeTopics()
    {
        return $this->belongsToMany(FeeTopic::class, 'faculty_fee_topic');
    }
}

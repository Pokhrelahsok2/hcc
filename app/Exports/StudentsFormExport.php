<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentsFormExport implements WithHeadings
{
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'First Name',
            'Middle Name',
            'Last Name',
            'Date Of Birth',
            'Address',
            'Current Term',
        ];
    }
}

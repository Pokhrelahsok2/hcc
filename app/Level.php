<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    public function faculties()
    {
        return $this->hasMany(Faculty::class);
    }
}

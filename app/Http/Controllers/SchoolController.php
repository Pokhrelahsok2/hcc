<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

use Illuminate\Support\Facades\Storage;
use Plank\Mediable\MediaUploaderFacade;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.schools.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|',
            'principal_name' => 'required',
            'address' => 'required',
            'established_in' => 'required',
        ]);

        $school =  School::updateOrCreate(
            ['id' => $id],
            [
                'name' => $request['name'],
                'address' => $request['address'],
                'established_id' => $request['established_id'],
                'principal_name' => $request['principal_name'],
                'established_in' => $request['established_in'],
                'slogan' => $request['slogan'],
                'description' => $request['description'],
            ]
        );

        $media = $school->getMedia('default')->first();
        if ($request->hasFile('logo')) {
            if ($media) {
                Storage::delete($media->getDiskPath());
                $media->delete();
            }
            $media = MediaUploaderFacade::fromSource($request->file('logo'))->useFilename("School_Logo")->upload();
            $school->attachMedia($media, 'default');
        }

        return redirect()->back()->with('success', 'School Information Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Student;
use App\Batch;
use Illuminate\Http\Request;
use App\Exports\StudentsFormExport;
use App\Imports\StudentsImport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $batches = Batch::all();
        $students = Student::latest();

        if (count($request->all()) < 1) $students = $students->take(10);
        else {
            if ($request->has('name')) {
                $name = $request->get('name');
                $students = $students->where('first_name', 'like', '%' . $name . '%')->orWhere('middle_name', 'like', '%' . $name . '%')
                    ->orWhere('last_name', 'like', '%' . $name . '%')->orWhere('address', 'like', '%' . $name . '%');
            }
            if ($request->has('batch')) {
                $students = $students->where('batch_id', $request->get('batch'));
            }
            if ($request->has('current_term')) {
                $students = $students->where('current_term', $request->get('current_term'));
            }
        }
        $students = $students->get();
        return view('pages.students.index', compact('batches', 'students', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'current_term' => 'required|integer|min:1|max:16',
            'batch_id' => 'required|exists:batches,id'
        ]);

        $student = new Student();
        $student->first_name = $request->get('first_name');
        $student->last_name = $request->get('last_name');
        $student->address = $request->get('address');
        $student->middle_name = $request->get('middle_name') ?? null;
        $student->current_term = $request->get('current_term');
        $student->dob = $request->get('dob');
        $student->batch_id = $request->get('batch_id');

        $student->save();

        return redirect()->back()->with('success', 'Student Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        if (!$student) {
            return redirect()->back()->with('error', 'The requested Student doesnt exist!');
        }
        $batches = Batch::all();
        return view('pages.students.edit', compact('student', 'batches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        if (!$student) {
            return redirect()->back()->with('error', 'The Student Doesnt Exist');
        }
        $student->delete();

        return redirect()->back()->with('success', 'The Student has been deleted Successfully!');
    }
    public function downloadForm()
    {
        return Excel::download(new StudentsFormExport(), 'students.xlsx');
    }
    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx',
            'batch' => 'required|exists:batches,id'
        ]);

        $students = Excel::toArray(new StudentsImport,  request()->file('file'))[0];
        if (count($students) < 1) return redirect()->back()->with('warning', 'No Data Given in Excel File!');

        $errors = 0;
        foreach ($students as $row) {
            if (!$row['first_name'] || !$row['last_name']) {
                $errors++;
                continue;
            }
            $student = new Student([
                'first_name' => ucwords($row['first_name']),
                'middle_name' =>  ucwords($row['middle_name']) ?? null,
                'last_name' =>  ucwords($row['last_name']),
                'dob' => Date::excelToDateTimeObject($row['date_of_birth'])->format('Y-m-d') ?? null,
                'batch_id' => $request->get('batch'),
                'address' =>  ucwords($row['address']) ?? null,
                'current_term' => $row['current_term'] ?? 1,
            ]);
            $student->save();
        }
        return redirect()->back()->with($errors > 0 ? 'warning' : 'success', 'Excel imported Successfully with error in ' . $errors . ' Rows');
    }
}

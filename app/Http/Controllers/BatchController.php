<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Batch;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::all();
        $batches = Batch::all();
        return view('pages.batches.index', compact('faculties', 'batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'year_started' => 'required|integer|min:2000|max:3000',
            'year_ended' => 'required|integer|min:2000|max:3000',
            'start_date' => 'required|date',
            'total_fee' => 'required|integer|min:1',
            'faculty_id' => 'required|exists:faculties,id',
            'fee_topics' => 'required',
        ]);
        $batch = new Batch();
        $batch->faculty_id = $request->get('faculty_id');
        $batch->year_start = $request->get('year_started');
        $batch->total_fee = $request->get('total_fee');
        $batch->year_end = $request->get('year_ended');
        $batch->started_at = $request->get('start_date');
        $batch->save();

        return redirect()->back()->with('success', 'Batch added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function show(Batch $batch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit(Batch $batch)
    {
        if (!$batch) {
            return redirect()->back()->with('warning', 'The Batch you wanted to edit does not exist.');
        }

        $faculties = Faculty::all();
        return view('pages.batches.edit', compact('batch', 'faculties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batch $batch)
    {
        $this->validate($request, [
            'year_started' => 'required|integer|min:2000|max:3000',
            'year_ended' => 'required|integer|min:2000|max:3000',
            'start_date' => 'required|date',
            'faculty_id' => 'required|exists:faculties,id',
            'total_fee' => 'required|integer|min:1',
        ]);
        $batch->faculty_id = $request->get('faculty_id');
        $batch->total_fee = $request->get('total_fee');
        $batch->year_start = $request->get('year_started');
        $batch->year_end = $request->get('year_ended');
        $batch->started_at = $request->get('start_date');
        $batch->save();


        return redirect()->action('BatchController@index')->with('success', 'Batch updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batch $batch)
    {
        if ($batch) {
            $batch->delete();
        }

        return redirect()->action('BatchController@index')->with('success', 'Batch deleted successfully!');
    }
}

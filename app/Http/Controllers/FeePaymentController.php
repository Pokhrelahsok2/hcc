<?php

namespace App\Http\Controllers;

use App\FeePayment;
use Illuminate\Http\Request;

class FeePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeePayment  $feePayment
     * @return \Illuminate\Http\Response
     */
    public function show(FeePayment $feePayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeePayment  $feePayment
     * @return \Illuminate\Http\Response
     */
    public function edit(FeePayment $feePayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeePayment  $feePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeePayment $feePayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeePayment  $feePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeePayment $feePayment)
    {
        //
    }
}

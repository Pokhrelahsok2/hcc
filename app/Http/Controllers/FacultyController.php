<?php

namespace App\Http\Controllers;


use App\Faculty;
use App\Level;
use App\FeeTopic;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::all();
        $levels = Level::all();
        $feeTopics = FeeTopic::all();
        return view('pages.faculties.index', compact('faculties', 'levels', 'feeTopics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'term_period' => 'required|integer|min:1|max:24',
            'level_id' => 'required|exists:levels,id',
            'name' => 'required|unique:faculties,name',
            'total_terms' => 'required|integer|min:0|max:16',
            'fee_topics' => 'required',
            'fee_topics.*' => 'exists:fee_topics,id'
        ]);
        $faculty = new Faculty();
        $faculty->name = $request->get('name');
        $faculty->level_id = $request->get('level_id');
        $faculty->term_period = $request->get('term_period');
        $faculty->total_terms = $request->get('total_terms');
        $faculty->save();

        $faculty->feeTopics()->sync($request->get('fee_topics'));

        return redirect()->back()->with('success', 'Faculty added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty)
    {
        if (!$faculty) {
            return redirect()->back()->with('warning', 'The Faculty you wanted to edit does not exist.');
        }

        $levels = Level::all();
        $feeTopics = FeeTopic::all();
        $selectedTopics = $faculty->feeTopics->pluck('id')->toArray();
        return view('pages.faculties.edit', compact('faculty', 'levels', 'feeTopics', 'selectedTopics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faculty $faculty)
    {
        $this->validate($request, [
            'term_period' => 'required|integer|min:1|max:24',
            'level_id' => 'required|exists:levels,id',
            'name' => 'required|unique:faculties,name,' . $faculty->id,
            'total_terms' => 'required|integer|min:0|max:16',
            'fee_topics' => 'required',
            'fee_topics.*' => 'exists:fee_topics,id'
        ]);

        $faculty->name = $request->get('name');
        $faculty->level_id = $request->get('level_id');
        $faculty->term_period = $request->get('term_period');
        $faculty->total_terms = $request->get('total_terms');
        $faculty->save();

        $faculty->feeTopics()->sync($request->get('fee_topics'));

        return redirect()->action('FacultyController@index')->with('success', 'Faculty updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faculty $faculty)
    {
        if ($faculty) {
            $faculty->delete();
        }

        return redirect()->action('FacultyController@index')->with('success', 'Faculty deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\FeeTopic;
use Illuminate\Http\Request;

class FeeTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeTopics = FeeTopic::all();

        return view("pages.fee-topics.index", compact('feeTopics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:fee_topics,name'
        ]);

        $topic = new FeeTopic();
        $topic->name = $request->get("name");
        $topic->save();

        return redirect()->back()->with("success", 'Fee Topic Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeeTopic  $feeTopic
     * @return \Illuminate\Http\Response
     */
    public function show(FeeTopic $feeTopic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeeTopic  $feeTopic
     * @return \Illuminate\Http\Response
     */
    public function edit(FeeTopic $topic)
    {
        if (!$topic) {
            return redirect()->back()->with("error", 'The Requested Fee Topic does not exist!');
        }

        return view("pages.fee_topics.edit", compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeeTopic  $feeTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeeTopic $topic)
    {
        $this->validate($request, [
            'name' => 'required|unique:fee_topics,name,' . $topic->id,
        ]);

        $topic->name = $request->get("name");
        $topic->save();

        return redirect()->back()->with("success", 'Fee Topic Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeeTopic  $feeTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeeTopic $feeTopic)
    {
        if ($feeTopic) {
            $feeTopic->delete();
        }
        return redirect()->back()->with("success", 'Fee Topic Deleted Successfully!');
    }
}

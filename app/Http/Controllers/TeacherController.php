<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view('pages.teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|min:2|max:30',
            'last_name' => 'required|string|min:2|max:30',
            'salary' => 'required',

        ]);
        $teacher = new Teacher();
        $teacher->first_name = $request->get('first_name');
        $teacher->middle_name = $request->get('middle_name');
        $teacher->last_name = $request->get('last_name');
        $teacher->salary = $request->get('salary');
        $teacher->save();

        return redirect()->back()->with('success', 'Teacher added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        if (!$teacher) {
            return redirect()->back()->with('warning', 'The Teacher you wanted to edit does not exist.');
        }


        return view('pages.teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $this->validate($request, [
            'first_name' => 'required|string|min:2|max:30',
            'last_name' => 'required|string|min:2|max:30',
            'salary' => 'required',

        ]);
        $teacher->first_name = $request->get('first_name');
        $teacher->middle_name = $request->get('middle_name');
        $teacher->last_name = $request->get('last_name');
        $teacher->salary = $request->get('salary');
        $teacher->save();

        return redirect()->action('TeacherController@index')->with('success', 'Teacher updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        if ($teacher) {
            $teacher->delete();
        }

        return redirect()->action('TeacherController@index')->with('success', 'Teacher deleted successfully!');
    }
}

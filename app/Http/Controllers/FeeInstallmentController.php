<?php

namespace App\Http\Controllers;

use App\FeeInstallment;
use Illuminate\Http\Request;

class FeeInstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeeInstallment  $feeInstallment
     * @return \Illuminate\Http\Response
     */
    public function show(FeeInstallment $feeInstallment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeeInstallment  $feeInstallment
     * @return \Illuminate\Http\Response
     */
    public function edit(FeeInstallment $feeInstallment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeeInstallment  $feeInstallment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeeInstallment $feeInstallment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeeInstallment  $feeInstallment
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeeInstallment $feeInstallment)
    {
        //
    }
}

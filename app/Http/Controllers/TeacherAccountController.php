<?php

namespace App\Http\Controllers;

use App\TeacherAccount;
use Illuminate\Http\Request;

class TeacherAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TeacherAccount  $teacherAccount
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherAccount $teacherAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeacherAccount  $teacherAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherAccount $teacherAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeacherAccount  $teacherAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeacherAccount $teacherAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeacherAccount  $teacherAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherAccount $teacherAccount)
    {
        //
    }
}

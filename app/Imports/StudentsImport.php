<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Importable;

class StudentsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    use Importable;
    public function model(array $row)
    {
        return new Student([
            'first_name' => $row['first_name'],
            'middle_name' => $row['middle_name'],
            'last_name' => $row['last_name'],
            'dob' => Date::excelToDateTimeObject($row['date_of_birth'])->format('Y-m-d'),
            'address' => $row['address'],
            'current_term' => $row['current_term']
        ]);
    }
}

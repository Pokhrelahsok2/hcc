<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class School extends Model
{
    use Mediable;
    protected $fillable = ['id', 'name', 'principal_name', 'description', 'slogan', 'established_in', 'address'];
}

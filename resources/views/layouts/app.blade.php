<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Argon Dashboard') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('argon') }}/css/nepali-datepicker.css" rel="stylesheet">
    <!-- Argon CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
    <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body class="{{ $class ?? '' }}">
    @auth()
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    @include('layouts.navbars.sidebar')
    @endauth

    <div class="main-content">
        @include('layouts.navbars.navbar')
        @yield('content')
    </div>

    @guest()
    @include('layouts.footers.guest')
    @endguest



    <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/js/nepali-datepicker.js"></script>
    <script>
        function getNepaliDate (date){
            if(!date) date = new Date();
            let d = calendarFunctions.getBsDateByAdDate(date.getFullYear(), date.getMonth()+1,
            date.getDate());
            let formatedNepaliDate = calendarFunctions.getBsMonthInfoByBsDate(d.bsYear,d.bsMonth,d.bsDate,"%D, %M %d, %y");
            return formatedNepaliDate;
        }
    </script>

    @stack('js')

    <!-- Argon JS -->
    <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>
    <script>
        String.prototype.capitalize = function() {
            return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };
        $(document).ready(function() {
            $.fn.select2.defaults.set( "theme", "bootstrap" );
            $(".datepicker-today").datepicker({ format: 'yyyy-mm-dd', }).datepicker('setDate',new Date());
            $(".datepicker").datepicker({ format: 'yyyy-mm-dd', });
            
            $(".nepali-datepicker").nepaliDatePicker({
                dateFormat: "%D, %M %d, %y",
                closeOnDateSelect: true,
            });

            //adding date to the navbar
            $("#date_now").text(getNepaliDate().formattedDate+" / "+new Date().toISOString().split('T')[0]);

            
            //handling nepali and english date conversion
            $(".BsToAd").each(function(){
                let AdDate = $(this).val();
                if(AdDate){
                    let d = getNepaliDate(new Date(AdDate));
                    $("#"+$(this).data('parent-id')).val(d.formattedDate);
                }
            })
            $(".nepali-datepicker").on("dateChange",function(){
                let nepaliDate = calendarFunctions.parseFormattedBsDate("%D, %M %d, %y",$(this).val());
                english_date = calendarFunctions.getAdDateByBsDate(nepaliDate.bsYear,nepaliDate.bsMonth,nepaliDate.bsDate+1);
                english_date = new Date(english_date).toISOString().split("T")[0];
                $("#"+($(this).data('target-id'))).val(english_date);
            })

            //capatitalize fata on sub,it
            $('form').on('submit',function(){
                $('.text-capitalize').each(function(){
                    console.log($(this).val());
                    $(this).val($(this).val().capitalize(true));
                })
            })

            //datatable

            $("#data-table").DataTable();

            //year picker
            $(".yearpicker").datepicker({
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years"
            });

            //change data inside table from ad to bs
            $(".AdToBs").each(function(){
                if($(this).prop('tagName') == 'INPUT'){
                    let ad = $(this).val();
                    if(ad) $('#'+$(this).data('parent-id')).val(getNepaliDate(new Date(ad)).formattedDate);
                    return;
                }
                let ad = $(this).text();
                if(ad) $(this).text(getNepaliDate(new Date(ad)).formattedDate);
            })

            //adding required mark to required terms
            $('input,textarea,select').filter('[required]').each(function(){
                console.log($(this));
                var $label = $("label[for='"+$(this).attr('id')+"']")
                if($label) $label.append('<span class="text-danger"> *</span>');
            })

            //select2
            $(".select2").select2();

        });
    </script>

    <script>
        $(document).ready(function() {
                toastr.options = {
                    "positionClass" : "toast-top-center",
                    "closeButton" : true,
                    "debug" : false,
                    "newestOnTop" : true,
                    "progressBar" : true,
                    "preventDuplicates" : false,
                    "onclick" : null,
                    "showDuration" : "300",
                    "opacity":1,
                    "hideDuration" : "1000",
                    "timeOut" : "4000",
                    "extendedTimeOut" : "1000",
                    "showEasing" : "swing",
                    "hideEasing" : "linear",
                    "showMethod" : "fadeIn",
                    "hideMethod" : "fadeOut"
                }

                @if(Session::has('success'))
                    toastr['success']("{{ Session::get('success') }}")
                @endif

                @if(Session::has('warning'))
                    toastr['warning']("{{ Session::get('warning') }}")
                @endif

                @if(Session::has('error'))
                    toastr['error']("{{ Session::get('error') }}")
                @endif
            });
    </script>
    @yield('scripts')
</body>

</html>
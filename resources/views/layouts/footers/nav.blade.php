<div class="copyright text-center text-xl-left pl-3">
    &copy; {{ now()->year }} <a href="http://alphatech.com.np/" class="font-weight-bold ml-1" target="_blank">Alphatech
        Hetauda</a>
    {{-- <a href="https://www.updivision.com" class="font-weight-bold ml-1" target="_blank">Updivision</a> --}}
</div>
@extends('layouts.app', ['title' => __('Dashboard')])

@section('content')
{{-- @include('layouts.headers.cards') --}}

{{-- <div class="container-fluid mt--7"> --}}
<div class="header bg-gradient-primary pt-md-6 pb-1">
    <div class="container-fluid">

        <!-- Card stats -->
    </div>
</div>
@include('layouts.footers.auth')
@endsection

{{-- @push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush --}}
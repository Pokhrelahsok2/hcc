@extends('layouts.app', ['title' => __('Add/View Teachers')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            {{-- <a href="{{ action('StaffController@dashboard') }}">
            <button class="btn btn-icon btn-sm btn-success active" type="button">
                <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                <span class="btn-inner--text">Go back</span>

            </button>

            </a> --}}
            <a data-toggle="collapse" href="#add-faculty" aria-expanded="true" aria-controls="test-block">
                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                    <span class="btn-inner--text">Create a new Teacher</span>

                </button>
            </a>
        </div>
        <div id="add-faculty" class="collapse {{$errors->isEmpty() ? '':'show'}}">
            <div class="card-block p-4">
                <form action="{{ action('TeacherController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" class="form-control text-capitalize"
                                    id="first_name" required>
                                @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                                <label for="middle_name">Middle Name</label>
                                <input type="text" name="middle_name" class="form-control text-capitalize"
                                    id="middle_name">
                                @if ($errors->has('middle_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('middle_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                <label for="last_name">Last Name</label>
                                <input type="text" name="last_name" class="form-control text-capitalize" id="last_name"
                                    required>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="salary">Monthly Salary(Rs)</label>
                                <input type="number" name="salary" min=0 id="salary" required class="form-control">
                                @if ($errors->has('salary'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('salary') }}</strong>
                                </span>
                                @endif
                                {{-- <textarea rows="3" name="remarks" id="remarks" class="form-control"
                                    placeholder="About this staff..."></textarea> --}}
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Teacher</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th>S.N</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Deleted at</th>
                        <th>Salary</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($teachers as $key=>$teacher)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{$teacher->first_name}}</td>
                        <td>{{ $teacher->middle_name }}</td>
                        <td>{{ $teacher->last_name }}</td>
                        <td>{{ $teacher->deleted_at }}</td>
                        <td>{{ $teacher->salary }}</td>

                        <td>
                            <form action="{{ action('TeacherController@destroy', $teacher->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm"
                                    href="{{ action('TeacherController@edit', $teacher->id) }}">
                                    Edit
                                </a>
                                <button class="btn btn-sm btn-danger">
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
@extends('layouts.app', ['title' => __('Edit Teacher Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('TeacherController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>

                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('TeacherController@update',1) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label for="first_name">First Name</label>
                            <input type="text" value="{{$teacher->first_name}}" name="first_name"
                                class="form-control text-capitalize" id="first_name" required>
                            @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                            <label for="middle_name">Middle Name</label>
                            <input type="text" value="{{$teacher->middle_name}}" name="middle_name"
                                class="form-control text-capitalize" id="middle_name" required>
                            @if ($errors->has('middle_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('middle_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <label for="last_name">Last Name</label>
                            <input type="text" value="{{$teacher->last_name}}" name="last_name"
                                class="form-control text-capitalize" id="last_name" required>
                            @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="form-group">
                            <label for="salary">Salary(Rs)</label>
                            <input value="{{$teacher->salary}}" type="number" name="salary" min=0 id="salary" required
                                class="form-control">
                            @if ($errors->has('salary'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('salary') }}</strong>
                            </span>
                            @endif
                            {{-- <textarea rows="3" name="remarks" id="remarks" class="form-control"
                                placeholder="About this staff..."></textarea> --}}
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Update Teacher</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
@extends('layouts.app', ['title' => __('Add/View Batches')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            {{-- <a href="{{ action('StaffController@dashboard') }}">
            <button class="btn btn-icon btn-sm btn-success active" type="button">
                <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                <span class="btn-inner--text">Go back</span>

            </button>

            </a> --}}
            <a data-toggle="collapse" href="#add-faculty" aria-expanded="true" aria-controls="test-block">
                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                    <span class="btn-inner--text">Create a new Batch</span>

                </button>
            </a>
        </div>
        <div id="add-faculty" class="collapse {{$errors->isEmpty() ? '':'show'}}">
            <div class="card-block p-4">
                <form action="{{ action('BatchController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('faculty_id') ? ' has-danger' : '' }}">
                                <label for="faculty_id">Faculty Name</label>
                                <select required name="faculty_id" id="faculty_id" class="form-control">
                                    <option value="">Select A Faculty</option>
                                    @foreach($faculties as $faculty)
                                    <option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('faculty_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('faculty_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="form-group{{ $errors->has('year_started') ? ' has-danger' : '' }}">
                                <label for="year_started">Starting Year (B.S)</label>
                                <input type="number" name="year_started" id="year_started" required class="form-control"
                                    value="2076" min="2000" max="3000">
                                @if ($errors->has('year_started'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year_started') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="form-group{{ $errors->has('start_date') ? ' has-danger' : '' }}">
                                <label for="start_date_bs">Batch Started At</label>
                                <input type="text" data-target-id="start_date_ad" id="start_date_bs" required
                                    autocomplete="off" class="form-control nepali-datepicker">
                                <input type="hidden" name="start_date" id="start_date_ad"
                                    data-parent-id="start_date_bs">
                                @if ($errors->has('start_date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-2 col-12">
                            <div class="form-group{{ $errors->has('year_ended') ? ' has-danger' : '' }}">
                                <label for="year_ended">Ending Year (B.S)</label>
                                <input type="number" name="year_ended" id="year_ended" required class="form-control"
                                    value="2080" min="2000" max="3000">
                                @if ($errors->has('year_ended'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('year_ended') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-12">
                            <div class="form-group{{ $errors->has('total_fee') ? ' has-danger' : '' }}">
                                <label for="total_fee">Total Fee (R.S)</label>
                                <input type="number" name="total_fee" id="total_fee" required class="form-control"
                                     min="1">
                                @if ($errors->has('total_fee'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('total_fee') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Batch</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th>S.N</th>
                        <th>Faculty</th>
                        <th>Year Started</th>
                        <th>Started Date</th>
                        <th>Year Ended</th>
                        <th>Total Fee</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($batches as $key=>$batch)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $batch->faculty->name }}</td>
                        <td>{{ $batch->year_start . ' B.S'}}</td>
                        <td class="AdToBs">{{ $batch->started_at}}</td>
                        <td>{{ $batch->year_end . ' B.S'}}</td>
                        <td>{{ $batch->total_fee.' R.S' }}</td>
                        <td>
                            <form action="{{ action('BatchController@destroy', $batch->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm" href="{{ action('BatchController@edit', $batch->id) }}">
                                    Edit
                                </a>
                                <button class="btn btn-sm btn-danger">
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
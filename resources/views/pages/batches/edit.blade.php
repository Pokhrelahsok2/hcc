@extends('layouts.app', ['title' => __('Edit Batch Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('BatchController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>

                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('BatchController@update',1) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('faculty_id') ? ' has-danger' : '' }}">
                            <label for="faculty_id">Faculty Name</label>
                            <select required name="faculty_id" id="faculty_id" class="form-control">
                                <option value="">Select A Faculty</option>
                                @foreach($faculties as $faculty)
                                <option {{$batch->faculty_id == $faculty->id ? 'selected' : ''}}
                                    value="{{ $faculty->id }}">
                                    {{ $faculty->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('faculty_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('faculty_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('year_started') ? ' has-danger' : '' }}">
                            <label for="year_started">Starting Year</label>
                            <input type="number" name="year_started" id="year_started" required class="form-control"
                                value="{{$batch->year_start}}" min="2000" max="3000">
                            @if ($errors->has('year_started'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('year_started') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('start_date') ? ' has-danger' : '' }}">
                            <label for="start_date_bs">Batch Started At</label>
                            <input type="text" data-target-id="start_date_ad" id="start_date_bs" required
                                autocomplete="off" class="form-control nepali-datepicker">
                            <input type="hidden" name="start_date" id="start_date_ad" class="AdToBs"
                                data-parent-id="start_date_bs" value="{{$batch->started_at}}">
                            @if ($errors->has('start_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('year_ended') ? ' has-danger' : '' }}">
                            <label for="year_ended">Ending Year</label>
                            <input type="number" name="year_ended" id="year_ended" required class="form-control"
                                value="{{$batch->year_end}}" min="2000" max="3000">
                            @if ($errors->has('year_ended'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('year_ended') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('total_fee') ? ' has-danger' : '' }}">
                            <label for="total_fee">Total Fee (R.S)</label>
                        <input type="number" name="total_fee" id="total_fee" required class="form-control" value="{{$batch->total_fee}}"
                                min="1">
                            @if ($errors->has('total_fee'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('total_fee') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Update Batch</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
@extends('layouts.app', ['title' => __('Add/View Levels')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            {{-- <a href="{{ action('StaffController@dashboard') }}">
            <button class="btn btn-icon btn-sm btn-success active" type="button">
                <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                <span class="btn-inner--text">Go back</span>

            </button>

            </a> --}}
            <a data-toggle="collapse" href="#add-level" aria-expanded="true" aria-controls="test-block">
                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                    <span class="btn-inner--text">Create a new Level</span>

                </button>
            </a>
        </div>
        <div id="add-level" class="collapse {{$errors->isEmpty() ? '':'show'}}">
            <div class="card-block p-4">
                <form action="{{ action('LevelController@store') }}" method="post">
                @csrf
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group required{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label for="name">Level Name </label>
                                <input type="text" required name="name" id="name" placeholder="Enter Level Name"
                                    class="form-control text-capitalize">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 text-right pt-sm-4">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Level</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    {{-- <tr>
                        <th colspan="7" class="with-heading">
                            <span class="page-small-heading">
                                All Staffs (<a href="{{ action('StaffController@export') }}">View All <strong
                        class="text-danger">Attendance</strong> In Excel</a>)
                    </span>
                    </th>
                    </tr> --}}
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($levels as $key=>$level)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $level->name }}</td>
                        <td>
                            <form action="{{ action('LevelController@destroy', $level->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm" href="{{ action('LevelController@edit', $level->id) }}">
                                    Edit
                                </a>
                                <button class="btn btn-sm btn-danger">
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>@include('layouts.footers.auth')
        
    </div>
</div>
@endsection
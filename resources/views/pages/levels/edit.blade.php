@extends('layouts.app', ['title' => __('Edit School Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('LevelController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>

                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('LevelController@update',$level->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label for="name">Level Name</label>
                            <input required value="{{ $level->name ?? '' }}" type="text" name="name" id="name"
                                class="form-control">

                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-12 text-right pt-sm-4">
                        <div class="form-group">
                            <button class="btn btn-primary">Update Level Name</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
@extends('layouts.app', ['title' => __('Edit School Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('HomeController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>
                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('SchoolController@update',1) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label for="name">Name</label>
                            <input required value="{{ $school->name ?? '' }}" type="text" name="name" id="name"
                                class="form-control text-capitalize">

                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                            <label for="address">Address</label>
                            <input required value="{{ $school->address ?? ''}}" type="text" name="address" id="address"
                                class="form-control text-capitalize">

                            @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('established_in') ? ' has-danger' : '' }}">
                            <label for="established_in">Established In</label>
                            <input required type="text" id="established_in" class="form-control nepali-datepicker"
                                data-target-id="AdDate" autocomplete="off">

                            <input type="hidden" name="established_in" id="AdDate" data-parent-id="established_in"
                                class="BsToAd" value="{{$school->established_in ?? ''}}">

                            @if ($errors->has('established_in'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('established_in') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('principal_name') ? ' has-danger' : '' }}">
                            <label for="principal_name">Principal Name</label>
                            <input required value="{{ $school->principal_name ?? '' }}" type="text"
                                name="principal_name" id="principal_name" class="form-control text-capitalize">

                            @if ($errors->has('principal_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('principal_name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('slogan') ? ' has-danger' : '' }}">
                            <label for="slogan">Slogan</label>
                            <input value="{{ $school->slogan ?? '' }}" type="text" name="slogan" id="slogan"
                                class="form-control text-capitalize">

                            @if ($errors->has('slogan'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('slogan') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                            <label for="description">Description</label>
                            <input value="{{ $school->description ?? '' }}" type="text"
                                name="description text-capitalize" id="description" class="form-control">

                            @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 col-12">
                        <div class="form-group{{ $errors->has('logo') ? ' has-danger' : '' }}">
                            <label for="logo">Logo</label>
                            <input value="{{ $school->logo ?? '' }}" type="file" name="logo" id="logo"
                                class="form-control">

                            @if ($errors->has('logo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('logo') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    @if($school && $school->getMedia('default')->first())
                    <div class="col-md-4 col-12">
                        <a href="{{ $school->getMedia('default')->first()->getUrl() }}">
                            <img src="{{ $school->getMedia('default')->first()->getUrl() }}" alt="Expense Image"
                                class="img-fluid" style="max-width: 100px;">
                        </a>
                    </div>

                    @endif

                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Update School Details</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
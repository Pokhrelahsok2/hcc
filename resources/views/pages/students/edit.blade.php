@extends('layouts.app', ['title' => __('Edit Student Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('StudentController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>

                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('FacultyController@update',$student->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                <label for="first_name">First Name</label>
                            <input type="text" name="first_name" class="form-control text-capitalize" value="{{$student->first_name}}" id="first_name" required>
                                @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                                <label for="middle_name">Middle Name</label>
                                <input value="{{$student->middle_name}}" type="text" name="middle_name" class="form-control text-capitalize" id="middle_name">
                                @if ($errors->has('middle_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('middle_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                <label for="last_name">Last Name</label>
                                <input value="{{$student->last_name}}" type="text" name="last_name" class="form-control text-capitalize" id="last_name" required>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                <label for="address">Address</label>
                                <input value="{{$student->address}}" type="text" name="address" class="form-control text-capitalize" id="address">
                                @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-3 col-12">
                            <div class="form-group{{ $errors->has('batch_id') ? ' has-danger' : '' }}">
                                <label for="batch_id">Batch</label>
                                <select required name="batch_id" id="batch_id" class="form-control">
                                    <option value="">Select Batch</option>
                                    @foreach($batches as $batch)
                                    <option value="{{ $batch->id }}" {{$student->batch_id == $batch->id ? 'selected':''}}>
                                        {{ $batch->faculty->name." :".$batch->year_start }}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('batch_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('batch_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('current_term') ? ' has-danger' : '' }}">
                                <label for="current_term">Current Term</label>
                                <input value="{{$student->current_term}}" type="number" name="current_term" class="form-control text-capitalize" id="current_term" value="1" min="1" max="16" required>
                                @if ($errors->has('current_term'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('current_term') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('dob') ? ' has-danger' : '' }}">
                                <label for="dob">Date Of Birth</label>
                                <input type="text" id="dob-bs" class="form-control nepali-datepicker" data-target-id="dob-ad" id="dob" autocomplete="off">
                                <input value="{{$student->dob}}" type="hidden" name="dob" id="dob-ad" data-parent-id="dob-bs" class="AdToBs">
                                @if ($errors->has('dob'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('dob') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Add Student</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
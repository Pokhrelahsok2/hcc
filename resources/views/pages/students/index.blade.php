@extends('layouts.app', ['title' => __('Add/View Students')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-2 col-sm-12 pt-2">
                    <a data-toggle="collapse" href="#add-faculty" aria-expanded="true" aria-controls="test-block">
                        <button class="btn btn-icon btn-sm btn-primary active" type="button">
                            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                            <span class="btn-inner--text">Create a new Student</span>

                        </button>
                    </a>
                </div>
                <div class="col-md-10 col-sm-12">
                    <form action="{{action('StudentController@importExcel')}}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-end">
                            <div class="col-md-3 col-sm-6 col-12">
                                <a href="{{action('StudentController@downloadForm')}}">
                                    <button class="btn btn-icon btn-md btn-danger" type="button">
                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                        <span class="btn-inner--text">Download Excel Form</span>
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="form-group{{ $errors->has('file') ? ' has-danger' : '' }}">
                                    <input required type="file" name="file" id="file" class="form-control"  accept="excel/xls, excel/xlxs"
                                        placeholder="Choose Excel File">
                                    @if ($errors->has('file'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="form-group{{ $errors->has('batch') ? ' has-danger' : '' }}">
                                    <select required name="batch" id="batch" class="form-control">
                                        <option value="">Select Batch</option>
                                        @foreach($batches as $batch)
                                        <option value="{{ $batch->id }}">
                                            {{ $batch->faculty->name." :".$batch->year_start }}
                                        </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('batch'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('batch') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-12 text-right">
                                <div class="form-group">

                                    <button class="btn btn-icon btn-md btn-default form-group">
                                        <span class="btn-inner--icon"><i class="ni ni-cloud-upload-96"></i></span>

                                        <span class="btn-inner--text">Import Data From Excel</span>

                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="add-faculty" class="collapse {{$errors->isEmpty() ? '':'show'}}">
            <div class="card-block p-4">
                <form action="{{ action('StudentController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" class="form-control text-capitalize" id="first_name" required>
                                @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                                <label for="middle_name">Middle Name</label>
                                <input type="text" name="middle_name" class="form-control text-capitalize" id="middle_name">
                                @if ($errors->has('middle_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('middle_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                                <label for="last_name">Last Name</label>
                                <input type="text" name="last_name" class="form-control text-capitalize" id="last_name" required>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                <label for="address">Address</label>
                                <input type="text" name="address" class="form-control text-capitalize" id="address">
                                @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-12">
                            <div class="form-group{{ $errors->has('batch_id') ? ' has-danger' : '' }}">
                                <label for="batch_id">Batch</label>
                                <select required name="batch_id" id="batch_id" class="form-control">
                                    <option value="">Select Batch</option>
                                    @foreach($batches as $batch)
                                    <option value="{{ $batch->id }}">
                                        {{ $batch->faculty->name." :".$batch->year_start }}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('batch_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('batch_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('current_term') ? ' has-danger' : '' }}">
                                <label for="current_term">Current Term</label>
                                <input type="number" name="current_term" class="form-control text-capitalize" id="current_term" value="1" min="1" max="16" required>
                                @if ($errors->has('current_term'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('current_term') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('dob') ? ' has-danger' : '' }}">
                                <label for="dob">Date Of Birth</label>
                                <input type="text" id="dob-bs" class="form-control nepali-datepicker" data-target-id="dob-ad" id="dob" autocomplete="off">
                                <input type="hidden" name="dob" id="dob-ad" data-parent-id="dob-bs">
                                @if ($errors->has('dob'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('dob') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Add Student</button>
                        </div>
                    </div>
                </form>
            </div>
        </form>
    </div>
    @if ($students->count()>0)
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <form action="{{action('StudentController@index')}}" method="get"
                enctype="multipart/form-data">
                {{-- @csrf --}}
                <div class="row justify-content-end">
                    <div class="col-md-3 col-sm-3 col-12">
                        <div class="form-group">
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                </div>
                                <input class="form-control" placeholder="Search Name , Address" type="text" name="name" value="{{$request->get('name')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-12">
                        <div class="form-group">
                            <select name="batch" id="batch" class="form-control">
                                <option value="">Select Batch</option>
                                @foreach($batches as $batch)
                                <option value="{{ $batch->id }}"  {{$batch->id == $request->get('batch') ? 'selected' :''}}>
                                    {{ $batch->faculty->name." :".$batch->year_start }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-12">
                        <div class="form-group">
                            <input type="number" max="16" value="{{$request->get('current_term')}}" placeholder="Current Term" name="current_term" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-1 col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-success">Search</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    @endif
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Batch</th>
                    <th>Address</th>
                    <th>Date Of Birth</th>
                    <th>Current Term</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($students as $key=>$student)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{$student->first_name ." ".$student->last_name}}</td>
                    <td>{{ $student->batch->faculty->name ." : ".$student->batch->year_start }}</td>
                    <td>{{ $student->address }}</td>
                    <td class="AdToBs">{{ $student->dob }}</td>
                    <td>{{ $student->current_term.' of '. $student->batch->faculty->total_terms }}</td>

                    <td>
                        <form action="{{ action('StudentController@destroy', $student->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-info btn-sm" href="{{ action('StudentController@edit', $student->id) }}">
                                Edit
                            </a>
                            <button class="btn btn-sm btn-danger">
                                <span class="fa fa-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
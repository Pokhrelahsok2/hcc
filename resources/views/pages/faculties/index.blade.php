@extends('layouts.app', ['title' => __('Add/View Faculties')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            {{-- <a href="{{ action('StaffController@dashboard') }}">
            <button class="btn btn-icon btn-sm btn-success active" type="button">
                <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                <span class="btn-inner--text">Go back</span>

            </button>

            </a> --}}
            <a data-toggle="collapse" href="#add-faculty" aria-expanded="true" aria-controls="test-block">
                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                    <span class="btn-inner--text">Create a new Faculty</span>

                </button>
            </a>
        </div>
        <div id="add-faculty" class="collapse {{$errors->isEmpty() ? '':'show'}}">
            <div class="card-block p-4">
                <form action="{{ action('FacultyController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label for="name">Faculty Name</label>
                                <input type="text" name="name" class="form-control text-capitalize" id="name" required>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('level_id') ? ' has-danger' : '' }}">
                                <label for="level_id">Level Name</label>
                                <select required name="level_id" id="level_id" class="form-control">
                                    <option value="">Select A Level</option>
                                    @foreach($levels as $level)
                                    <option value="{{ $level->id }}">{{ $level->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('level_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('level_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group{{ $errors->has('term_period') ? ' has-danger' : '' }}">
                                <label for="term_period">Each Term Period (Months)</label>
                                <input required type="number" name="term_period" id="term_period" class="form-control" value="6" min="1" max="24">
                                @if ($errors->has('term_period'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('term_period') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="total_terms">Total Terms</label>
                                <input type="number" name="total_terms" min=0 id="total_terms" required value="8"
                                    class="form-control">
                                @if ($errors->has('total_terms'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('total_terms') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 col-12">
                            <div class="form-group{{ $errors->has('fee_topics') ? ' has-danger' : '' }}">
                                <label for="fee_topics">Applicable Fee Topics</label>
                                <select data-placeholder="Select Applicable Topics" multiple multiple="multiple" required name="fee_topics[]" id="fee_topics" class="form-control select2">
                                    <option value="">Select A Faculty</option>
                                    @foreach($feeTopics as $topic)
                                    <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('fee_topics'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fee_topics') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Faculty</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Level</th>
                        <th>Term Period</th>
                        <th>Total Terms</th>
                        <th>Total Period</th>
                        <th>Fee Topics</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($faculties as $key=>$faculty)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{$faculty->name}}</td>
                        <td>{{ $faculty->level->name }}</td>
                        <td>{{ $faculty->term_period . ' Months' }}</td>
                        <td>{{ $faculty->total_terms }}</td>
                        <td>{{ ($faculty->total_terms * $faculty->term_period) /12 . ' Years' }}</td>
                        <td>
                            @foreach ($faculty->feeTopics as $topic)
                                <button disabled="disabled" class="btn-primary">{{$topic->name}}</button>
                            @endforeach
                        </td>

                        <td>
                            <form action="{{ action('FacultyController@destroy', $faculty->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm"
                                    href="{{ action('FacultyController@edit', $faculty->id) }}">
                                    Edit
                                </a>
                                <button class="btn btn-sm btn-danger">
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
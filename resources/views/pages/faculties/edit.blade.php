@extends('layouts.app', ['title' => __('Edit Faculty Information')])

@section('content')
@include('users.partials.header')
<div>
    <div class="card">
        <div class="card-header pt-4">
            <a href="{{ action('FacultyController@index') }}">
                <button class="btn btn-icon btn-sm btn-success active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                    <span class="btn-inner--text">Go back</span>

                </button>
            </a>
        </div>
        <div class="card-block p-4">
            <form action="{{ action('FacultyController@update',$faculty->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label for="name">Faculty Name</label>
                            <input type="text" value="{{$faculty->name}}" name="name"
                                class="form-control text-capitalize" id="name" required>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('level_id') ? ' has-danger' : '' }}">
                            <label for="level_id">Level Name</label>
                            <select required name="level_id" id="level_id" class="form-control">
                                <option value="">Select A Level</option>
                                @foreach($levels as $level)
                                <option {{$faculty->level_id == $level->id ? 'selected' : ''}} value="{{ $level->id }}">
                                    {{ $level->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('level_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('level_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="form-group{{ $errors->has('term_period') ? ' has-danger' : '' }}">
                            <label for="term_period">Each Term Period (Months)</label>
                            <input required type="number" name="term_period" id="term_period" class="form-control"
                                value="{{$faculty->term_period}}" min="1" max="24">
                            @if ($errors->has('term_period'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('term_period') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="form-group">
                            <label for="total_terms">Total Terms</label>
                            <input value="{{$faculty->total_terms}}" type="number" name="total_terms" min=0
                                id="total_terms" required class="form-control">
                            @if ($errors->has('total_terms'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('total_terms') }}</strong>
                            </span>
                            @endif
                            {{-- <textarea rows="3" name="remarks" id="remarks" class="form-control"
                                placeholder="About this staff..."></textarea> --}}
                        </div>
                    </div>
                    <div class="col-md-12 col-12">
                        <div class="form-group{{ $errors->has('fee_topics') ? ' has-danger' : '' }}">
                            <label for="fee_topics">Applicable Fee Topics</label>
                            <select multiple multiple="multiple" required name="fee_topics[]" id="fee_topics"
                                class="form-control select2">
                                @foreach($feeTopics as $topic)
                                <option {{in_array($topic->id,$selectedTopics) ? 'selected':''}}
                                    value="{{ $topic->id }}">
                                    {{ $topic->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('fee_topics'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('fee_topics') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <div class="form-group">
                            <button class="btn btn-primary">Update Faculties</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('layouts.footers.auth')
    </div>
</div>
@endsection
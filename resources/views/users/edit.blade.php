@extends('layouts.app', ['title' => __('Users')])

@section('content')
@include('users.partials.header', [
'title' => __('Hello') . ' '. auth()->user()->name,
'class' => 'col-lg-7'
])

<div>
    <div class="col">
        <div class="card-shadow">
            <div class="row">
                <div class="col-xl-12 order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header">
                            <a href="{{ action('UserController@index') }}">
                                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                                    <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>

                                    <span class="btn-inner--text">Go back</span>

                                </button>

                            </a>
                            <strong><strong>Edit: <span class="text-muted">{{ $user->name }}</span></strong></strong>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ action('UserController@update', $user) }}"
                                autocomplete="off">
                                @csrf
                                @method('put')
                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                        <input type="text" name="name" id="input-name"
                                            class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('Name') }}" value="{{ old('name', $user->name) }}"
                                            required autofocus>

                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                        <input type="email" name="email" id="input-email"
                                            class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('Email') }}" value="{{ old('email', $user->email) }}"
                                            required>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                        <label class="form-control-label"
                                            for="input-role">{{ __('Select Role') }}</label>
                                        <select id="inputState" name="role" class="form-control">
                                            <option {{ $user->role == 0 ?  'selected':''}} value="0">Supervisor</option>
                                            <option {{ $user->role == 1 ?  'selected':''}} value="1">Admin</option>
                                        </select>
                                        @if ($errors->has('role'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('contact') ? ' has-danger' : '' }}">
                                        <label class="form-control-label"
                                            for="input-contact">{{ __('Contact') }}</label>
                                        <input type="text" name="contact_number" id="input-contact"
                                            class="form-control form-control-alternative{{ $errors->has('contact') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('contact') }}" value="{{$user->contact_number}}">

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label class="form-control-label"
                                            for="input-password">{{ __('Password (Leave it empty to keep it Unchanged!)') }}</label>
                                        <input type="password" name="password" id="input-password"
                                            class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            placeholder="{{ __('Password') }}" value="">

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"
                                            for="input-password-confirmation">{{ __('Confirm Password') }}</label>
                                        <input type="password" name="password_confirmation"
                                            id="input-password-confirmation"
                                            class="form-control form-control-alternative"
                                            placeholder="{{ __('Confirm Password') }}" value="">
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

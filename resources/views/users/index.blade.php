@extends('layouts.app', ['title' => __('Users')])

@section('content')
@include('users.partials.header', [
'title' => __('Hello') . ' '. auth()->user()->name,
'class' => 'col-lg-7'
])

<div>
    <div class="card">
        <div class="card-header">
            <a data-toggle="collapse" href="#add-site-block" aria-expanded="true" aria-controls="test-block">
                <button class="btn btn-icon btn-sm btn-primary active" type="button">
                    <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>

                    <span class="btn-inner--text">Create a new User</span>

                </button>
            </a>

        </div>
        <div id="add-site-block" class="collapse">
            <div class="card-block p-4">
                <form action="{{ action('UserController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        {{-- <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="text" name="date" id="date" placeholder=""
                                        class="form-control datepicker" autocomplete="off">
                                </div>
                            </div> --}}

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="item">Name</label>
                                <input type="text" name="name" id="item" required class="form-control">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="item">Email</label>
                                <input type="text" name="email" id="item" required class="form-control">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="item">Contact Number</label>
                                <input type="text" name="contact_number" id="item" class="form-control">
                                @if ($errors->has('contact_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('contact_number') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="item">Password</label>
                                <input type="password" name="password" id="item" required class="form-control">
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="inputState">Select Role</label>
                                <select id="inputState" name="role" class="form-control">
                                    <option selected value="0">Supervisor</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        </div>

                        {{-- <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="amount">Amount (Rs.)</label>
                                    <input type="number" name="amount" id="amount" required class="form-control">
                                </div>
                            </div> --}}
                        <div class="col-12 text-right">
                            <div class="form-group">
                                <button class="btn btn-primary">Add User</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col">{{ __('Email') }}</th>
                        <th scope="col">{{ __('Contact Number') }}</th>
                        <th scope="col">{{ __('Role') }}</th>
                        <th scope="col">{{ __('Creation Date') }}</th>
                        <th scope="col">{{__('Actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>
                            <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                        </td>
                        <td>
                            {{ $user->contact_number }}
                        </td>
                        <td>
                            {{ $user->role ==1? "Admin" : "Supervisor" }}
                        </td>
                        <td>{{ $user->created_at->format('d/m/Y H:i') }}</td>
                        <td>
                            <form action="{{ action('UserController@destroy', $user->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm" href="{{ action('UserController@edit', $user->id) }}">
                                    Edit
                                </a>
                                <button class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                        {{-- <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        @if ($user->id != auth()->id())
                                        <form action="{{ route('user.destroy', $user) }}" method="post">
                        @csrf
                        @method('delete')

                        <a class="dropdown-item" href="{{ route('user.edit', $user) }}">{{ __('Edit') }}</a>
                        <button type="button" class="dropdown-item"
                            onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                            {{ __('Delete') }}
                        </button>
                        </form>
                        @else
                        <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Edit') }}</a>
                        @endif
        </div>
    </div>
    </td> --}}
    </tr>
    @endforeach
    </tbody>
    </table>
</div>
{{-- <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $users->links() }}
</nav>
</div> --}}
</div>
@endsection
